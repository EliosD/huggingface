import React, { Component } from "react";
import './search.css';

class Search extends Component {
    state = {
		searchValue: '',
		readmes: null,
		pageCount: 0,
		currentPage: 0,
		maxItemsPerPage: window.maxSearchPages
	};
	
	renderPagingButtons = () => {
		return <div>
		   {
				this.state.currentPage > 0 ?
				<button class="buttonCls" id="buttonBack" onClick={() => this.reloadPage('back')}>back</button>
				: null}
		   {
				this.state.pageCount > (this.state.currentPage + 1) * this.state.maxItemsPerPage ?
				<button class="buttonCls" id="buttonNext" onClick={() => this.reloadPage('next')}>next</button>
				: null
		   }
		   </div>;
	 }
	 
	reloadPage = (direction) => {
		if (direction === 'back') {
			this.state.currentPage--;
		} else if (direction === 'next') {
			this.state.currentPage++;
		}
		this.makeApiCall(this.state.searchValue, this.state.currentPage);
	 }
	
	handleOnChange = event => {
		this.setState({ searchValue: event.target.value });
	};
	handleSearch = () => {
		this.state.currentPage = 0;
		this.makeApiCall(this.state.searchValue, this.state.currentPage);
	}
	makeApiCall = (searchInput, page) => {
		let searchUrl = `/get_readmes?k=${searchInput}&p=${page}`;
		fetch(searchUrl)
		.then(response => {
			return response.json();
		})
		.then(jsonData => {
			this.setState({ readmes: jsonData.result });
			this.setState({ pageCount: jsonData.page_count });
		});
	};
	cleanUrl = url => {
		return url.replace("raw/main/README.md","")
	}
	cleanReadme = readme => {
		return readme.replace(/[\r\n]+/g, '\n').replace(/(?:\r\n|\r|\n)/g, '<br>').replace(/(<br>)+/g, '<br>');
	}
	
    render() {
        return (
			<div id="container">
				<h1 id="title1">
					Welcome to HuggingFace's Search Application
				</h1>
				<h3>
					Browse for HuggingFace libraries using keywords
				</h3>
				<input name="text" type="text" placeholder="Type search words" class="inputCls" onChange={event => this.handleOnChange(event)} value={this.state.searchValue} />
				<button class="buttonCls" onClick={this.handleSearch}>
					Search
				</button>
				{this.state.readmes && this.state.readmes.length > 0 ? (
					<div>
						{this.state.readmes.map((readme, index) =>(
							<div class="listItems" key={index}>
								<h2 class="urlsList"><a href={this.cleanUrl(readme[0])}>{this.cleanUrl(readme[0])}</a></h2>
								<h3 class="readmeList" dangerouslySetInnerHTML={{__html: this.cleanReadme(readme[1])}}></h3>
							</div>
						))}
					</div>
				) : (
				<p>
					{this.state.readmes && this.state.readmes.length === 0 && this.state.currentPage === 0 ? (<p>Try typing a valid keyword</p>) : null}
				</p>
				)}
				{this.renderPagingButtons()}
			</div>
		);
    }
}

export default Search;