class Config:
    # Change this variable to get more results per search page
    max_urls_per_pages = 5

    # static urls
    url_api_list = "https://huggingface.co/api/models"
    domain = "https://huggingface.co/"
    suffix = "/raw/main/README.md"
