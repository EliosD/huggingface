import operator
from scraper import scrape
from search import engine
from flask import Flask, request, jsonify, render_template
from flask_cors import CORS, cross_origin
import operator
from config import *
from lru import LRU
from werkzeug.exceptions import HTTPException
import json

app = Flask(__name__, template_folder="static")

# Handle CORS to get requests from different domains
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# Build cache of 10 queries to not calculate similarities at each query. 
# Especially useful for request with a lot of pages
cache_dict = LRU(10)

@app.route('/get_readmes', methods=['GET'])
@cross_origin()
def process():
    docs = scrape.get_clean_readmes()

    # parameters are passed as query strings
    keyword = request.args.get('k')
    page = request.args.get('p')
    page = 0 if page == None else page

    if keyword not in cache_dict:
        df = engine.build_term_document_matrix(docs)
        similarities = engine.get_best_readmes(keyword, df, docs)
        # If query not cached, cache it
        cache_dict[keyword] = list(similarities)

    res = [docs[x] for x in map(operator.itemgetter(0), cache_dict[keyword])]

    # To change number of max urls returned in a page, change the variable max_urls_per_pages in config.py
    return jsonify({
        "page_count": len(res),
        # Handle pagination server-side, this is fast thanks to caching
        "result": res[int(page) * Config.max_urls_per_pages : (int(page)+1) * Config.max_urls_per_pages]})

@app.route('/', methods=['GET'])
def default():
    try:
        return render_template("index-react.html", flask_max_res_per_page = Config.max_urls_per_pages)
    except:
        return render_template("error.html")

@app.errorhandler(HTTPException)
def handle_error(error):
    """Handle HTTP errors in Api calls"""

    resp = error.get_response()
    
    # build error response
    resp.data = json.dumps({
        "code": error.code,
        "name": error.name,
        "description": error.description,
    })
    resp.content_type = "application/json"
    return resp

@app.errorhandler(Exception)
def handle_exception(error):
    """Handle All exceptions"""

    if isinstance(error, HTTPException):
        # HTTPExceptions are already handled
        return error

    # now you're handling non-HTTP exceptions only
    return render_template("error.html", error=error), 500


if __name__ == '__main__':
    app.run(debug=True, port=5000)
