from typing import *
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import numpy as np

vectorizer = TfidfVectorizer()

def build_term_document_matrix(cleaned_text: List[Tuple[str, str, str]]):
    """build the DataFrame that corresponds to the term-document matrix"""
    
    # Fit and transform data into a vector
    cleaned_text = [x[2] for x in cleaned_text]
    X = vectorizer.fit_transform(cleaned_text)
    
    # Convert it as transposed matrix
    X = X.T.toarray()

    # Create DataFrame and put vocabulary as index
    df = pd.DataFrame(X, index=vectorizer.get_feature_names())

    return df

def filter_non_zero_and_nan(tu):
    """Given an input tuple, filter out NaN and 0.0 floats of first position"""

    return not np.isnan(tu[1]) and tu[1] > 0.0

def get_best_readmes(query, df, docs):
    """Get best Readmes for a specific query and given a term-document matrix (df)"""

    q = [query]
    q_vec = vectorizer.transform(q).toarray().reshape(df.shape[0],)

    sim = {}
    # Calculate the similarity
    for i in range(len(docs)):
        sim[i] = np.dot(df.loc[:, i].values, q_vec) / np.linalg.norm(df.loc[:, i]) * np.linalg.norm(q_vec)

    # Sort the values
    sim_sorted = sorted(sim.items(), key=lambda x: x[1], reverse=True)
    sim_sorted_filter = filter(filter_non_zero_and_nan, sim_sorted)

    return sim_sorted_filter
    