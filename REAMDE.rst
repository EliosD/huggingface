HuggingFace Search Application
==============================

Requirements:
    ``python 3.7``

    ``python devel``

    ``pipenv``

    ``gcc``

    ``npm 7.6``

To run this app, first run the following commands to handle dependencies in a virtualenv
    ``pipenv install --ignore-pipfile``

    ``pipenv shell``

Then go the react-app and build the react Application

    ``cd react-app``

    ``npm install``

    ``npm run build``

You will see a file called ``index-react.html`` appear in the ``static`` folder at the root of the project.

Go back to the root of the project, and run (with python 3.7)

    ``python app.py``

Then on your browser go to http://localhost:5000 to start searching for HuggingFace libraries by keywords

To test, simply run:
    ``pytest``

Note that if you need an update list of HuggingFace projects, you need to run the following command (*warning this may take several hours*)

    ``cd scraper``

    ``python scrape.py``

You can then repeat the above process.

Front-end code is mostly contained inside ``react-app/src/search.js``

