from typing import *
import requests
import pathlib
import pickle
import os
import re
import string
from config import *

def get_build_urls(debug = False) -> List[str]:
    """ Build url list from huggingface.co """

    # get url list from somewhere like https://huggingface.co/api/models
    resp = requests.get(Config.url_api_list)
    
    try:
        #build urls that are not marked as private
        urls = {Config.domain + model['modelId'] + Config.suffix for model in resp.json() if not model['private']}
        urls = list(urls)
    except:
        urls = []

    return urls if not debug else urls[:30]

def get_readmes(urls: List[str]) -> List[Tuple[str, str]]:
    """ Build readmes from input urls """

    readmes = []
    count = 0

    for url in urls:
        resp = requests.get(url)

        if not count%10:
            print(url, resp.text)

        count +=1 

        # if there is no Readme, ignore result
        if resp.status_code != 404:
            readmes.append((url, resp.text))

    return readmes

def clean_text(raw_text : List[Tuple[str, str]]) -> List[Tuple[str, str, str]]:
    """Clean input list of strings before being processed"""

    cleaned_text = []
    for text in raw_text:
        # clean HTML tags
        clean = re.sub('<.*?>', ' ', text[1])

        # Remove punctuation
        clean = re.sub(f'[{re.escape(string.punctuation)}]', ' ', clean)

        # Remove end of line character
        clean = re.sub('\n', ' ', clean)
        
        # Remove numbers
        clean = re.sub('[0-9]', '', clean)

        # Put everything in lower case
        clean = clean.lower()

        # Filter non printable characters
        clean = ''.join(filter(lambda x: x in string.printable, clean))

        # Remove 2 or more spaces
        clean = re.sub(r'\s{2,}', ' ', clean)

        cleaned_text.append((text[0], text[1], clean))

    return cleaned_text

def build_filename(fn):
    """Build file name with corresponding"""

    file_path = pathlib.Path(__file__).parent / "static"
    return os.path.join(file_path, fn)

def main(debug = False):
    """If this file is executed, it will fetch the latest URLs and Readmes from huggingface.co.
    Warning this could take many minutes/hours. Set the debug flag to True to get only 30 results.
    """

    fn_urls = build_filename("urls.pkl")
    fn_rm = build_filename("readmes.pkl")
    fn_cl_rm = build_filename("clean_readmes.pkl")
    
    # If Debug flag is set, take the 30 first urls
    urls = get_build_urls(debug)
    # Save urls in a static file
    with open(fn_urls, "wb") as f:
        pickle.dump(urls, f)

    readmes = get_readmes(urls)
    # Save (url, raw_Readmes) in a static file
    with open(fn_rm, "wb") as f:
        pickle.dump(readmes, f)

    clean_readmes = clean_text(readmes)
    # Save (url, raw_Readmes, cleaned_Readmes) in a static file
    with open(fn_cl_rm, "wb") as f:
        pickle.dump(clean_readmes, f)

def get_clean_readmes():
    """ Get the Readmes corresponding the URL list in https://huggingface.co/api/models, from an already saved file.
    This does NOT scrape the huggingface.co web page and download the newtest content.
    """
    
    fn_cl_rm = build_filename("clean_readmes.pkl")    
    with open(fn_cl_rm, "rb") as f:
        clean_readmes = pickle.load(f)
    
    return clean_readmes

if __name__ == "__main__":
    main(debug = False)