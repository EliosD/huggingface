from scraper import scrape
from search import engine
import numpy as np

def test_clean_text_1():
    raw_list = [("url1", "A1\<div></div>15B2c)_:<>!/\\"),
                ("url2", "<h1>a<h1/>:<>!/B:<1238\x239>!/C)_:<99>!/\\\n")]
    res = scrape.clean_text(raw_list)

    assert res == [(raw_list[0][0], raw_list[0][1], "a bc "),
                   (raw_list[1][0], raw_list[1][1], " a b c ")]

def test_filter_non_zero_and_nan_1():
    input_tuple = (1, np.nan)
    assert not engine.filter_non_zero_and_nan(input_tuple)

def test_filter_non_zero_and_nan_2():
    input_tuple = (1, 0.0)
    assert not engine.filter_non_zero_and_nan(input_tuple)

def test_filter_non_zero_and_nan_3():
    input_tuple = (0, 0.0001)
    assert engine.filter_non_zero_and_nan(input_tuple)